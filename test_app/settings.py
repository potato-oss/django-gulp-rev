from __future__ import unicode_literals
DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'test.db',
    }
}
USE_TZ = False
SITE_ID = 1
SECRET_KEY = '{{secret_key}}'

STATIC_URL = '/static/'
STATIC_ROOT = ''

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_nose',
    'gulp_rev',
    'test_app',
)

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [
    '--with-xunit',
    '--xunit-file=nosetests.xml',
    '--with-coverage',
    '--cover-erase',
    '--cover-package=gulp_rev',
    '--cover-xml',
    '--cover-xml-file=coverage.xml',
]
